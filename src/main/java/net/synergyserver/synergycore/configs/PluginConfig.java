package net.synergyserver.synergycore.configs;

import net.synergyserver.synergycore.SynergyPlugin;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Used to access data in the config.yml configuration file in this plugin's directory.
 */
public class PluginConfig implements Config {

    private static PluginConfig instance = null;
    private static HashMap<String, YamlConfiguration> configs;

    /**
     * Creates a new <code>PluginConfig</code> object.
     */
    private PluginConfig() {
        configs = new HashMap<>();
    }

    /**
     * Returns the object representing this <code>PluginConfig</code>.
     *
     * @return The object of this class.
     */
    public static PluginConfig getInstance() {
        if (instance == null) {
            instance = new PluginConfig();
        }
        return instance;
    }

    public void load(SynergyPlugin plugin) {
        File file = new File(plugin.getDataFolder(), "config.yml");

        // If the file doesn't exist, make one with the default values
        if (!file.exists()) {
            plugin.saveResource("config.yml", false);
        }

        try {
            // Load the file from disk
            InputStreamReader input = new InputStreamReader(plugin.getResource("config.yml"));
            YamlConfiguration defaults = YamlConfiguration.loadConfiguration(input);
            input.close();

            // Add any missing values to the yaml configuration before saving it
            YamlConfiguration yaml = new YamlConfiguration();
            yaml.load(file);
            yaml.setDefaults(defaults);
            yaml.options().copyDefaults(true);

            // Store the config in the HashMap
            configs.put(plugin.getName(), yaml);

            // Update the file on disk
            save(plugin);
        } catch (IOException|InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void unload(SynergyPlugin plugin) {
        configs.remove(plugin.getName());
    }

    public void save(SynergyPlugin plugin) {
        try {
            getConfig(plugin).save(new File(plugin.getDataFolder(), "config.yml"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the <code>YamlConfiguration</code> object containing the given plugin's configuration.
     *
     * @param plugin The <code>SynergyPlugin</code> to get the config of.
     * @return The <code>YamlConfiguration</code> object containing this plugin's configuration.
     */
    public static YamlConfiguration getConfig(SynergyPlugin plugin) {
        return configs.get(plugin.getName());
    }

}
