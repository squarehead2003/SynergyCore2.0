package net.synergyserver.synergycore.configs;

import net.synergyserver.synergycore.SynergyPlugin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TextConfig {

    private static TextConfig instance = null;
    private static HashMap<String, List<String>> texts;

    /**
     * Creates a new <code>TextConfig</code> object.
     */
    private TextConfig() {}

    /**
     * Returns the object representing this <code>TextConfig</code>.
     *
     * @return The object of this class.
     */
    public static TextConfig getInstance() {
        if (instance == null) {
            instance = new TextConfig();
            texts = new HashMap<>();
        }
        return instance;
    }

    /**
     * Loads a text file from the given plugin.
     *
     * @param plugin The plugin to load the text file from.
     * @param fileName The name of the file.
     */
    public void load(SynergyPlugin plugin, String fileName) {
        File file = new File(plugin.getDataFolder(), fileName);

        // If the file doesn't exist, make one with the default values
        if (!file.exists()) {
            plugin.saveResource(fileName, false);
        }

        try {
            FileReader input = new FileReader(file);
            BufferedReader reader = new BufferedReader(input);
            List<String> text = new ArrayList<>();

            // Store the lines in the map
            while (reader.ready()) {
                String line = reader.readLine();

                // If for some reason the line is prematurely null then manually break out of the loop
                if (line == null) {
                    break;
                }

                // If the line is blank then replace it with a space so messaging players works
                if (line.isEmpty()) {
                    line = " ";
                }

                text.add(Message.translateCodes(line));
            }
            texts.put(fileName, text);

            reader.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Unloads the text with the given file name.
     *
     * @param fileName The name of the text to unload.
     */
    public void unload(String fileName) {
        texts.remove(fileName);
    }

    /**
     * Saves a text file for the given plugin into its data folder.
     *
     * @param plugin The plugin to save the text file for.
     * @param fileName The name of the file.
     */
    public void save(SynergyPlugin plugin, String fileName) {
        try {
            OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream(new File(plugin.getDataFolder(), fileName)));
            BufferedWriter writer = new BufferedWriter(output);
            List<String> text = texts.get(fileName);

            for (String line : text) {
                // Undo modifications made when loading when saving
                if (!line.equals(" ")) {
                    writer.write(line.replaceAll("§", "&"));
                }
                writer.newLine();
            }

            writer.close();
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the text of a loaded file.
     *
     * @param fileName The name of the file.
     * @return A list where each element is a line of the text.
     */
    public static List<String> getText(String fileName) {
        return texts.get(fileName);
    }
}
