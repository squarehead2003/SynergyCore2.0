package net.synergyserver.synergycore;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.UUID;

/**
 * Represents a teleport request, which is used by the "requesting" versions of the teleport commands.
 */
public class Teleport {

    private UUID sender;
    private UUID receiver;
    private Location teleportLocation;
    private TeleportType type;

    /**
     * Creates a new <code>Teleport</code> with the given parameters.
     *
     * @param sender The ID of the player who sent the request.
     * @param receiver The ID of the player who received the request.
     * @param teleportLocation The location of where the target should be teleported to.
     * @param type The <code>TeleportType</code> of this <code>Teleport</code>.
     */
    public Teleport(UUID sender, UUID receiver, Location teleportLocation, TeleportType type) {
        this.sender = sender;
        this.receiver = receiver;
        this.teleportLocation = teleportLocation;
        this.type = type;
    }

    /**
     * Gets the ID of the player who sent this request.
     *
     * @return The sender's UUID.
     */
    public UUID getSender() {
        return sender;
    }

    /**
     * Gets the ID of the player who received the request.
     *
     * @return The receiver's UUID.
     */
    public UUID getReceiver() {
        return receiver;
    }

    /**
     * Gets the location of where the target should be teleported to. If the type of this request is
     * <code>TeleportType.TO_RECEIVER</code> then the location of the receiver will be returned instead.
     *
     * @return The teleport location.
     */
    public Location getTeleportLocation() {
        if (type.equals(TeleportType.TO_RECEIVER)) {
            return Bukkit.getPlayer(receiver).getLocation();
        }
        return teleportLocation;
    }

    /**
     * Gets the <code>TeleportType</code> of this <code>Teleport</code>.
     *
     * @return The type of this <code>Teleport</code>.
     */
    public TeleportType getType() {
        return type;
    }

    /**
     * Gets the ID of the player to teleport.
     *
     * @return The teleporter's UUID.
     */
    public UUID getTeleporter() {
        UUID teleporter;

        switch (type) {
            case TO_RECEIVER:
                teleporter = sender;
                break;
            case TO_SENDER:
                teleporter = receiver;
                break;
            case EVERYONE_TO_SENDER:
                teleporter = receiver;
                break;
            default:
                teleporter = null;
                break;
        }
        return teleporter;
    }

    /**
     * Gets the ID of the player that is being teleported to.
     *
     * @return The teleportee's UUID.
     */
    public UUID getTeleportee() {
        UUID teleportee;

        switch (type) {
            case TO_RECEIVER:
                teleportee = receiver;
                break;
            case TO_SENDER:
                teleportee = sender;
                break;
            case EVERYONE_TO_SENDER:
                teleportee = sender;
                break;
            default:
                teleportee = null;
                break;
        }
        return teleportee;
    }
}
