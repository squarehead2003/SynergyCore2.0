package net.synergyserver.synergycore.settings;

import org.bukkit.Material;

/**
 * Represents a <code>ToggleSetting</code> handled by SynergyCore.
 */
public class CoreToggleSetting extends ToggleSetting {

    public static final CoreToggleSetting FULL_TELEPORTATION = new CoreToggleSetting("full_teleportation",
            SettingCategory.TELEPORTATION, "If switching worlds, teleports you to the intended destination instead " +
            "of to your last location in the targeted world.", "syn.setting.full-teleportation", true, false,
            Material.ENDER_PEARL);
    public static final CoreToggleSetting TELEPORTS = new CoreToggleSetting("teleports", SettingCategory.TELEPORTATION,
            "Allows players to teleport to you.", "syn.setting.teleports", true, true, Material.ENDER_PEARL);
    public static final CoreToggleSetting NOTIFY_TP_TARGET = new CoreToggleSetting("notify_tp_target",
            SettingCategory.TELEPORTATION, "Notifies your target when using /tp that you've teleported to them.",
            "syn.setting.notify-tp-target", true, true, Material.BOW);
    public static final CoreToggleSetting HOME_SHORTCUT = new CoreToggleSetting("home_shortcut",
            SettingCategory.TELEPORTATION, "Enables the /home to /home tp shortcut.", "syn.setting.home-shortcut", false, false,
            Material.WHITE_BED);

    public static final CoreToggleSetting RECEIVE_AFK_NOTIFICATIONS = new CoreToggleSetting("receive_afk_notifications",
            SettingCategory.CHAT, "Notifies you whenever a player's status changes.",
            "syn.setting.receive-afk-notifications", true, true, Material.PAINTING);
    public static final CoreToggleSetting EMIT_AFK_NOTIFICATIONS = new CoreToggleSetting("emit_afk_notifications",
            SettingCategory.CHAT, "Notifies other players whenever your AFK status changes.",
            "syn.setting.emit-afk-notifications", true, true, Material.INK_SAC);
    public static final CoreToggleSetting NAME_HIGHLIGHTING = new CoreToggleSetting("name_highlighting",
            SettingCategory.CHAT, "Highlights your name when it's mentioned in chat messages.",
            "syn.setting.name-highlighting", true, false, Material.NAME_TAG);
    public static final CoreToggleSetting COLORED_CHAT = new CoreToggleSetting("colored_chat",
            SettingCategory.CHAT, "Displays chat messages in their fully formatted and colored glory.",
            "syn.setting.colored-chat", true, true, Material.MAP);
    public static final CoreToggleSetting REMOVE_EXCESSIVE_CAPS = new CoreToggleSetting("remove_excessive_caps",
            SettingCategory.CHAT, "Forces messages sent to you to lowercases if they contain a large percentage of " +
            "capitalized letters.", "syn.setting.remove-excessive-caps", false, false, Material.LEATHER_HELMET);
    public static final CoreToggleSetting HIDE_SPAM = new CoreToggleSetting("hide_spam", SettingCategory.CHAT,
            "Prevents recently duplicated messages by the sender from being sent to you.", "syn.setting.hide-spam",
            false, false, Material.PORKCHOP);
    public static final CoreToggleSetting CONNECTION_MESSAGES = new CoreToggleSetting("connection_messages",
            SettingCategory.CHAT, "Shows join and leave messages for players.", "syn.setting.connection-messages",
            true, true, Material.SPRUCE_DOOR);
    public static final CoreToggleSetting MINIMAL_CHAT = new CoreToggleSetting("minimal_chat", SettingCategory.CHAT,
            "Hides all messages that don't target or mention you.", "syn.setting.minimal-chat", false, false,
            Material.PAPER);
    public static final CoreToggleSetting MINECHAT_JOIN_MESSAGE = new CoreToggleSetting("minechat_join_message",
            SettingCategory.CHAT, "If set to false, then this plugin will try to prevent MineChat's automatic join " +
            "message from being sent by you.", "syn.setting.minechat-join-message", false, false, Material.OAK_SIGN);
    public static final CoreToggleSetting EMIT_DEATH_MESSAGES = new CoreToggleSetting("emit_death_messages",
            SettingCategory.CHAT, "Broadcasts your death messages.", "syn.setting.death-messages",
            true, true, Material.BONE);

    public static final CoreToggleSetting JOIN_UNVANISHED = new CoreToggleSetting("join_unvanished",
            SettingCategory.SECRET, "Automatically unvanishes you when logging on the server.",
            "syn.setting.join-unvanished", false, false, Material.GLASS_BOTTLE);
    public static final CoreToggleSetting ANNOUNCE_QUIT = new CoreToggleSetting("announce_quit",
            SettingCategory.SECRET, "Announces that you've left the server if you leave while unvanished.",
            "syn.setting.announce-quit", true, true, Material.IRON_DOOR);
    public static final CoreToggleSetting CHAT_WHILE_VANISHED = new CoreToggleSetting("chat_while_vanished",
            SettingCategory.SECRET, "Allows chatting while vanished. Try not to blow your cover!",
            "syn.setting.chat-while-vanished", true, true, Material.POTION);
    public static final CoreToggleSetting STAFF_CHAT = new CoreToggleSetting("staff_chat", SettingCategory.SECRET,
            "Directs all of your chat messages to StaffChat.", "syn.staffchat", false, false, Material.ENCHANTED_BOOK);

    public static final CoreToggleSetting ALERT_SOUNDS = new CoreToggleSetting("alert_sounds", SettingCategory.OTHER,
            "Plays a sound whenever someone tries to get your attention " +
                    "(e.g. Teleport requests, mentioned in chat, etc)", "syn.setting.alert-sounds", true, false,
            Material.NOTE_BLOCK);
    public static final CoreToggleSetting AFK_TARGET_NOTIFICATION = new CoreToggleSetting("afk_target_notification",
            SettingCategory.OTHER, "Notifies you whenever the person you try to interact with is AFK.",
            "syn.setting.afk-target-notification", true, false, Material.BOW);
    public static final CoreToggleSetting ALLOW_SPECTATORS = new CoreToggleSetting("allow_spectators",
            SettingCategory.OTHER, "Allows other players to spectate you.", "syn.setting.allow-spectators", true, true,
            Material.PLAYER_HEAD);
    public static final CoreToggleSetting HELP_SHORTCUT = new CoreToggleSetting("help_shortcut", SettingCategory.OTHER,
            "Enables the /help to /help search shortcut.", "syn.setting.help-shortcut", false,
            false, Material.COMMAND_BLOCK);
    public static final CoreToggleSetting SETTINGS_SHORTCUT = new CoreToggleSetting("settings_shortcut", SettingCategory.OTHER,
            "Enables the /settings to /settings GUI shortcut.", "syn.setting.settings-shortcut", true,
            false, Material.MAP);

    private CoreToggleSetting(String id, SettingCategory category, String description, String permission,
                             boolean defaultValue, boolean defaultValueNoPermission, Material itemType) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission, itemType);
    }

}
