package net.synergyserver.synergycore.settings;

import org.bukkit.Material;

import java.util.function.Function;

/**
 * Represents a <code>MultiOptionSetting</code> handled by SynergyCore.
 */
public class CoreSetSetting<E> extends SetSetting<E> {

    public static final CoreSetSetting<String> NAME_MENTIONING_BLACKLIST = new CoreSetSetting<>("name_mentioning_blacklist",
            SettingCategory.CHAT, "Prevents words in chat from being detected as your name and mentioning you.",
            "syn.setting.name-mentioning-blacklist", new String[]{}, new String[]{}, Material.INK_SAC,
            String::toLowerCase, Function.identity());

    public static final CoreSetSetting<String> NAME_MENTIONING_WHITELIST = new CoreSetSetting<>("name_mentioning_whitelist",
            SettingCategory.CHAT, "Defines words in chat to be detected as your name and mention you.",
            "syn.setting.name-mentioning-whitelist", new String[]{}, new String[]{}, Material.BONE_MEAL,
            String::toLowerCase, Function.identity());

    private CoreSetSetting(String id, SettingCategory category, String description, String permission, String[] defaultValue,
                           String[] defaultValueNoPermission, Material itemType, Function<String, E> stringToElement,
                           Function<E, String> elementToString) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission, itemType, stringToElement, elementToString);
    }
}
