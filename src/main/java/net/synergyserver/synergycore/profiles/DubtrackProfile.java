package net.synergyserver.synergycore.profiles;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import io.sponges.dubtrack4j.framework.User;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.utils.DubtrackUtil;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the profile of a user on Dubtrack.
 */
@Entity(value = "dubtrackprofiles")
public class DubtrackProfile implements ConnectedProfile {

    @Property("sid")
    private ObjectId synID;

    @Id
    private String dtID;
    @Property("n")
    private String currentName;
    @Property("li")
    private long lastLogIn;
    @Property("lo")
    private long lastLogOut;
    @Property("b")
    private boolean banned;
    @Property("n1")
    private List<String> knownNames;

    @Transient
    private DataManager dm = DataManager.getInstance();

    /**
     * Required constructor for Morphia to work.
     */
    public DubtrackProfile() {}

    /**
     * Creates a new <code>DubtrackProfile</code> with the given variables.
     *
     * @param synID The synID of this user, used by Synergy to differentiate between people.
     * @param dtID The ID of this user, used by both Dubtrack and the database.
     * @param currentName The current name of this user.
     */
    public DubtrackProfile(ObjectId synID, String dtID, String currentName) {
        this.synID = synID;
        this.dtID = dtID;
        this.currentName = currentName;

        this.banned = false;
    }

    @Override
    public ObjectId getSynID() {
        return synID;
    }

    @Override
    public String getID() {
        return dtID;
    }

    /**
     * Gets the <code>User</code> represented by this <code>DubtrackProfile</code>, if they are currently in the room.
     *
     * @return The <code>User</code> for this <code>DubtrackProfile</code>.
     */
    public User getUser() {
        try {
            return SynergyCore.getDubtrackRoom().getUserById(dtID);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public String getCurrentName() {
        return currentName;
    }

    @Override
    public void setCurrentName(String newName) {
        currentName = newName;
        dm.updateField(this, DubtrackProfile.class, "n", newName);
    }

    @Override
    public long getLastLogIn() {
        return lastLogIn;
    }

    @Override
    public void setLastLogIn(long logInTime) {
        lastLogIn = logInTime;
        dm.updateField(this, DubtrackProfile.class, "li", logInTime);
    }

    @Override
    public long getLastLogOut() {
        return lastLogOut;
    }

    @Override
    public void setLastLogOut(long logOutTime) {
        lastLogOut = logOutTime;
        dm.updateField(this, DubtrackProfile.class, "lo", logOutTime);
    }

    /**
     * Checks if this Discord user is online.
     *
     * @return True if this user is online.
     */
    public boolean isOnline() {
        return (lastLogOut < lastLogIn) || DubtrackUtil.isOnline(dtID);
    }

    @Override
    public boolean isBanned() {
        return banned;
    }

    @Override
    public void setIsBanned(boolean banStatus) {
        banned = banStatus;
        dm.updateField(this, DubtrackProfile.class, "b", banStatus);
    }

    /**
     * Convenience method for adding a name to <code>knownNames</code>, if it's new.
     *
     * @param name The name to add to the list.
     * @return True if the name was not already added.
     */
    public boolean addKnownName(String name) {
        if (knownNames == null) {
            knownNames = new ArrayList<>();
        }

        if (knownNames.contains(name)) {
            return false;
        }

        knownNames.add(name);
        dm.updateField(this, DubtrackProfile.class, "nl", knownNames);
        return true;
    }
}
