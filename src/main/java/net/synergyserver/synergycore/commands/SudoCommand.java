package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "sudo",
        permission = "syn.sudo",
        usage = "/sudo <command|message>",
        description = "Main command for forcing players to perform an action."
)
public class SudoCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
