package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "gamemode",
        aliases = "gm",
        permission = "syn.gamemode",
        usage = "/gamemode <gamemode>",
        description = "Changes your gamemode.",
        minArgs = 1,
        maxArgs = 2
)
public class GamemodeCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player;
        GameMode gameMode;

        if (args.length == 1) {
            // If the sender is not a player, then there needs to be a player specified
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                sender.sendMessage(Message.get("commands.error.sender_type_requires_player"));
                return false;
            }
            // Otherwise set the player to the command sender
            player = (Player) sender;
        } else {
            // If they don't have permission to change the gamemodes of other players
            if (!sender.hasPermission("syn.gamemode.others")) {
                sender.sendMessage(Message.get("commands.gamemode.error.no_permission_other"));
                return false;
            }

            UUID pID = PlayerUtil.getUUID(args[1], false, sender.hasPermission("vanish.see"));

            // If no player was found then give the sender an error message
            if (pID == null) {
                sender.sendMessage(Message.format("commands.error.player_not_found", args[1]));
                return false;
            }
            player = Bukkit.getPlayer(pID);
        }

        // Check what gamemode they're referring to
        if (StringUtil.equalsIgnoreCase(args[0], "0", "s", "su", "sv", "survival")) {
            gameMode = GameMode.SURVIVAL;
        } else if (StringUtil.equalsIgnoreCase(args[0], "1", "c", "cr", "creative")) {
            gameMode = GameMode.CREATIVE;
        } else if (StringUtil.equalsIgnoreCase(args[0], "2", "a", "ad", "adventure")) {
            gameMode = GameMode.ADVENTURE;
        } else if (StringUtil.equalsIgnoreCase(args[0], "3", "sp", "spectator")) {
            gameMode = GameMode.SPECTATOR;
        } else {
            // If unable to match a gamemode then return an error
            sender.sendMessage(Message.format("commands.gamemode.error.gamemode_not_found", args[0]));
            return false;
        }

        String gamemodeName = gameMode.name().toLowerCase();

        // Check if the sender has permission to use that gamemode
        if (!sender.hasPermission("syn.gamemode." + gamemodeName)) {
            sender.sendMessage(Message.format("commands.gamemode.error.no_permission", gamemodeName));
            return false;
        }

        // Set the gamemode and give feedback
        player.setGameMode(gameMode);
        player.sendMessage(Message.format("commands.gamemode.info.gamemode_changed", gamemodeName));

        // If the sender is not the player then give them feedback too
        if (!sender.equals(player)) {
            sender.sendMessage(Message.format("commands.gamemode.info.gamemode_changed_other", player.getName(), gamemodeName));
        }
        return true;
    }
}
