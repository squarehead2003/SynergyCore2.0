package net.synergyserver.synergycore.database;

import com.mongodb.AggregationOptions;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClients;
import dev.morphia.Datastore;
import dev.morphia.Morphia;
import dev.morphia.mapping.DateStorage;
import dev.morphia.mapping.DiscriminatorFunction;
import dev.morphia.mapping.MapperOptions;
import dev.morphia.mapping.NamingStrategy;
import net.synergyserver.synergycore.MemberApplication;
import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.DubtrackProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.profiles.WebsiteProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.SettingDiffs;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.settings.SettingPreset;
import org.bson.UuidRepresentation;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * The class used to interface with the MongoDB database.
 */
public class MongoDB {

    private static MongoDB instance = null;
    private Morphia morphia;
    private Datastore datastore;

    /**
     * Creates a new <code>MongoDB</code> object.
     */
    private MongoDB() {}

    /**
     * Returns the object representing this <code>MongoDB</code>.
     *
     * @return The object of this class.
     */
    public static MongoDB getInstance() {
        if (instance == null) {
            instance = new MongoDB();
        }
        return instance;
    }

    /**
     * Connects to the database and prepares classes.
     */
    public void connect() {
        // Prepare the MongoClient
        YamlConfiguration config = SynergyCore.getPluginConfig();
        String uri = config.getString("database.uri");
        String dbname = config.getString("database.name");

        MapperOptions mapperOptions = MapperOptions.builder()
                .classLoader(SynergyCore.getPlugin().getMongoHack())
                .dateStorage(DateStorage.SYSTEM_DEFAULT) // Legacy
                .discriminatorKey("className") // Legacy
                .discriminator(DiscriminatorFunction.className()) // Legacy
                .collectionNaming(NamingStrategy.identity()) // Legacy
                .propertyNaming(NamingStrategy.identity()) // Legacy, but still default
                .build();
        MongoClientSettings clientSettings = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(uri))
                .uuidRepresentation(UuidRepresentation.JAVA_LEGACY)
                .build();

                // Create a Datastore to hold the database's data
        datastore = Morphia.createDatastore(MongoClients.create(clientSettings), dbname, mapperOptions);
        datastore.ensureIndexes();
    }

    /**
     * Gets the <code>Datastore</code> object used to interface with the database.
     *
     * @return The <code>Datastore</code> object used to interface with the database.
     */
    public Datastore getDatastore() {
        return datastore;
    }

    /**
     * Passes the given classes on to Morphia for object mapping for being saved
     * and retrieved to/from the database. All classes whose objects are to be
     * stored in the database in a non-binary data format must be mapped.
     *
     * @param classes The classes to map.
     */
    public void mapClasses(Class... classes) {
        datastore.getMapper().map(classes);
    }

    /**
     * This calls the map method on Morphia to prepare data classes for being saved
     * and retrieved to/from the database. All classes whose objects are to be
     * stored in the database in a non-binary data format must be mapped.
     */
    private void mapObjects() {
        // Map main DataEntities
        mapClasses(
                SynUser.class,
                DiscordProfile.class,
                DubtrackProfile.class,
                MinecraftProfile.class,
                WebsiteProfile.class,
                WorldGroupProfile.class,
                SettingPreferences.class,
                MemberApplication.class
        );
        // Also map embedded classes
        mapClasses(
                SerializableLocation.class,
                SettingDiffs.class,
                SettingPreset.class
        );
    }

    /**
     * Used as a temporary fix to MongoDB 3.6's new syntax for aggregation.
     * To be replaced with a less ghetto method in the future.
     */
    public static AggregationOptions getAggregationOptions() {
//        return AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        // TODO: check if still necessary?
        return AggregationOptions.builder().build();
    }
}
