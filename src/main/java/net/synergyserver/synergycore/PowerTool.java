package net.synergyserver.synergycore;

import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents an item that can run commands when right-clicked.
 */
@Embedded
public class PowerTool {

    @Property("m")
    private Material toolMaterial;
    @Property("d")
    private short durability;
    @Property("c")
    private List<String> commands = new ArrayList<>();

    @Transient
    private long lastUsed;

    /**
     * Required constructor for Morphia to work.
     */
    public PowerTool() {}

    /**
     * Creates a new <code>PowerTool</code> with the given parameters.
     *
     * @param toolMaterial The material of item of the powertool.
     * @param durability The durability of the item of the powertool.
     * @param command The first command of the powertool.
     */
    public PowerTool(Material toolMaterial, short durability, String command) {
        this.toolMaterial = toolMaterial;
        this.durability = durability;
        commands.add(command);
    }

    /**
     * Gets the material of the item of this powertool.
     *
     * @return The material of the item of this powertool.
     */
    public Material getToolMaterial() {
        return toolMaterial;
    }

    /**
     * Gets the durability of the item of this powertool.
     *
     * @return The durability of the item of this powertool.
     */
    public short getDurability() {
        return durability;
    }

    /**
     * Gets the list of commands of this powertool.
     *
     * @return The commands of this powertool.
     */
    public List<String> getCommands() {
        return commands;
    }

    /**
     * Adds a command to this powertool.
     *
     * @param command The command to add.
     */
    public void addCommand(String command) {
        commands.add(command);
    }

    /**
     * Removes a command from this powertool.
     *
     * @param index The index of the command to remove.
     */
    public void removeCommand(int index) {
        commands.remove(index);
    }

    /**
     * Gets the last time this powertool was used.
     *
     * @return The last time this powertool was used.
     */
    public long getLastUsed() {
        return lastUsed;
    }

    /**
     * Sets the last time this powertool was used.
     *
     * @param lastUsed The time to set.
     */
    public void setLastUsed(long lastUsed) {
        this.lastUsed = lastUsed;
    }
}
