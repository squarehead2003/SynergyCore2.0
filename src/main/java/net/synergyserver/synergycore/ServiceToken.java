package net.synergyserver.synergycore;

import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Property;
import net.synergyserver.synergycore.utils.StringUtil;

/**
 * Represents a token that is used to connect to one of the services handled by SynergyCore.
 */
@Embedded
public class ServiceToken {

    @Property("t")
    private String token;
    @Property("c")
    private long created;
    @Property("l")
    private int life;

    /**
     * Required constructor for Morphia to work.
     */
    public ServiceToken() {}

    /**
     * Creates a new <code>ServiceToken</code> with the given life.
     *
     * @param life The life of this <code>ServiceToken</code>, in milliseconds.
     */
    public ServiceToken(int life) {
        this.life = life;

        this.token = StringUtil.generateRandomString(4);
        this.created = System.currentTimeMillis();
    }

    /**
     * Gets the randomly-generated token that the user needs to use to connect a service.
     *
     * @return The token of this <code>ServiceToken</code>.
     */
    public String getToken() {
        return token;
    }

    /**
     * Gets the time that this <code>ServiceToken</code> was created at.
     *
     * @return The creation time of this <code>ServiceToken</code>.
     */
    public long getCreated() {
        return created;
    }

    /**
     * Gets the time, in milliseconds, after the creation time that this <code>ServiceToken</code> expires.
     *
     * @return The life of this <code>ServiceToken</code>.
     */
    public int getLife() {
        return life;
    }

    /**
     * Checks whether this <code>ServiceToken</code> has expired.
     *
     * @return True if this <code>ServiceToken</code> is no longer valid.
     */
    public boolean isExpired() {
        return System.currentTimeMillis() > created + life;
    }
}
