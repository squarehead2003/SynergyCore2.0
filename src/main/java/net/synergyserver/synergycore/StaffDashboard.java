package net.synergyserver.synergycore;

import dev.morphia.Datastore;
import dev.morphia.query.CountOptions;
import dev.morphia.query.experimental.filters.Filters;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;

/**
 * The main object that tracks and handles staff things.
 */
public class StaffDashboard {

    private List<MemberApplication> memberApplications;
    private static StaffDashboard instance = null;

    // TODO: REMOVE AND REPLACE WITH NONGHETTO CODE
    public boolean memberWhitelist = false;

    /**
     * Creates a new <code>StaffDashboard</code> object.
     */
    private StaffDashboard() {}

    /**
     * Returns the object representing this <code>StaffDashboard</code>.
     *
     * @return The object of this class.
     */
    public static StaffDashboard getInstance() {
        if (instance == null) {
            instance = new StaffDashboard();
        }
        return instance;
    }

    /**
     * Updates the stored list of <code>MemberApplication</code>s from the database and returns the result.
     *
     * @return All open <code>MemberApplication</code>.
     */
    public List<MemberApplication> getMemberApplications() {
        memberApplications = MongoDB.getInstance().getDatastore().find(MemberApplication.class)
                .stream().sorted(Comparator.comparingLong(MemberApplication::getTimeSent)).toList();
        return memberApplications;
    }

    /**
     * Gets a stored <code>MemberApplication</code> by its index in the list.
     *
     * @param index The index of the <code>MemberApplication</code> to get.
     * @return The requested <code>MemberApplication</code>, if found.
     */
    public MemberApplication getMemberApplication(int index) {
        if (index >= memberApplications.size()) {
            return null;
        }
        return memberApplications.get(index);
    }

    /**
     * Saves the given <code>MemberApplication</code> to the database
     * and resolves any other applications sent by that player.
     *
     * @param application The application.
     */
    public void saveMemberApplication(MemberApplication application) {
        // Delete the old application in the database
        resolveMemberApplication(application.getPlayerID());

        // Save the application to the database
        DataManager.getInstance().saveDataEntity(application);
    }

    /**
     * Deletes the current <code>MemberApplication</code> of the player, if it exists.
     */
    public void resolveMemberApplication(UUID pID) {
        Datastore ds = MongoDB.getInstance().getDatastore();
        ds.find(MemberApplication.class).filter(Filters.eq("pid", pID)).delete();
    }

    /**
     * Checks if the given player has a pending <code>MemberApplication</code>.
     *
     * @param pID The UUID of the player to check.
     * @return True if a matching document was found in the database.
     */
    public boolean hasPendingMemberApplication(UUID pID) {
        return MongoDB.getInstance().getDatastore().find(MemberApplication.class)
                .filter(Filters.eq("pid", pID))
                .count(new CountOptions().limit(1)) > 0;
    }

}
