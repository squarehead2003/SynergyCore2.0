package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.profiles.SynUser;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever a player's donation status is updated..
 */
public class DonatorStatusUpdateEvent extends Event {

    private SynUser donator;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>DonatorStatusUpdateEvent</code> with the given parameters.
     *
     * @param donator The <code>SynUser</code> of the donator.
     */
    public DonatorStatusUpdateEvent(SynUser donator) {
        this.donator = donator;
    }

    /**
     * Gets the <code>SynUser</code> of the donator.
     *
     * @return The <code>SynUser</code> of this event.
     */
    public SynUser getDonator() {
        return donator;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
